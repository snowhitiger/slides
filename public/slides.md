% Deep learning and quantum computing for nuclear physics
% Long-Gang Pang, IOPP@CCNU
% 2019-09-26, LanZhou IMP

---
header-includes: <script src="p5.js"></script><script src="reveal.js/plugin/math/math.js"></script> <link rel="stylesheet" href="user.css">
---

# What is deep learning

## A small subset of AI and ML

<img class="plain" src="images/DL_ML_AI_2.png" width="70%" align='center'/>

## Inspired by human brain

<img class="plain" src="images/brain_picture.png" width="60%" align='center'/>

For most people, deep learning = multi-layer artificial neural network

## One neuron of animal brain

<img class="plain" src="images/Neuron.png" alt="By User:Dhp1080 - Anatomy and Physiology by the US National Cancer Institute's Surveillance, Epidemiology and End Results (SEER) Program ., CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=1474927" width="60%" align='center'/>

## { data-background-iframe="anim/one_neuron/one_neuron.html"}

## 
<img class="plain" src="images/multiple_output_neurons.png" width="80%" align='center'/>

## 
<img class="plain" src="images/multiple_hidden_neuron.png" width="80%" align='center'/>

## 
<img class="plain" src="images/multiple_hidden_layers.png" width="80%" align='center'/>

## { data-background-iframe="http://playground.tensorflow.org"}

## How to train a neural network
<img class="plain" src="images/how_to_train.png" alt="drawing" width="70%" align='center'/>

## Dense net has too many links (parameters)

> Von Neumann: with four parameters I can fit an elephant,
> and with five I can make him wiggle his trunk.

## Ways to reduce overfitting
- Early stopping
- Train, validation, testing
- L1, L2 Regularization, weight decay ...
- More data
    a. accumulate more training data
    b. data augmentation (crop, scale, rotate ...)
- Fewer parameters
    a. Dropout, Drop connection
    b. Go deep. S.Liang & R.Srikant, arXiv:1610.04161
    c. **Convolution neural network**

## { data-background-iframe="anim/conv_1d/conv1d.html"}

## 2D convolution
<img class="plain" src="images/kernel_convolution.jpg" alt="drawing" width="60%" align='center'/>

## { data-background-iframe="https://snowhitiger.gitlab.io/conv2d/"}

## Convolution for policy and value estimation

<img class="plain" src="images/alphago_policy.png" alt="drawing" width="80%" align='center'/>

AlphaGo defeated human champion in 2016!

## AlphaGo Zero all by self-play

<img class="plain" src="images/alphago_zero.gif" alt="drawing" width="60%" align='center'/>

## Face identification (who)

<img class="plain" src="images/face_recog.jpg" alt="drawing" width="50%" align='center'/>

## Image segmentation (what and where)

<img class="plain" src="images/mask_rcnn.png" alt="drawing" width="80%" align='center'/>

## Phase of matter

<img class="plain" src="images/ML_for_phase_of_matter.png" alt="drawing" width="70%" align='center'/>

## Jet flavor classification

<img class="plain" src="images/deep_jet.png" alt="drawing" width="70%" align='center'/>

## Solving Schrodinger equation and DFT

<img class="plain" src="images/deep_schrodinger.png" alt="drawing" width="70%" align='center'/>


# DL for nuclear physics

## QCD equation of state 
<img class="plain" src="images/qcd_phase_diagram.png" alt="drawing" width="50%" align='center'/>

- Locate critical end point
- Classify 1st order and crossover

## QCD equation of state 
<img class="plain" src="images/qcd_transition_eos_1.png" alt="drawing" width="50%" align='center'/>

## Phase transition signal in QGP expansion
<img class="plain" src="images/hydro_evolution.png" alt="drawing" width="70%" align='center'/>
$$ \nabla_{\mu} T^{\mu\nu} = 0 $$

## Will the signal survive?

<img class="plain" src="images/feature_image.jpg" alt="drawing" width="70%" align='center'/>

<p class="ref"> LG. Pang, K.Zhou, N.Su, H.Petersen, H. Stoecker, XN. Wang. Nature Communications 9 (2018) no.1, 210 </p>

## EoS-meter using DL

<img class="plain" src="images/eos_meter.png" alt="drawing" width="70%" align='center'/>

## Generalize well on testing data

<img class="plain" src="images/cvs_vs_size_10fold_naturev3.png" alt="drawing" width="50%" align='center'/>

## Compare to traditional machine learning

<img class="plain" src="images/deepeos_traditional_ml.png" alt="drawing" width="50%" align='center'/>

- GROUP1: IEBE-VISHNU
- GROUP2: CLVisc + IPGlasma

## Which region is important for classification
<img class="plain" src="images/prediction_difference_analysis.png" alt="drawing" width="70%" align='center'/>


## Low transverse momentum regions are important

<img class="plain" src="images/importance_region_eos.png" alt="drawing" width="70%" align='center'/>

## Indistinguishable using other observables

<img class="plain" src="images/tradional_obs.png" alt="drawing" width="70%" align='center'/>

## Or correlations

<img class="plain" src="images/combine_scatter_plots.png" alt="drawing" width="40%" align='center'/>


## New task: classify Maxwell and Spinodal 

<img class="plain" src="images/Eos_diff.png" alt="drawing" width="50%" align='center'/>

<p class="ref"> J.~Steinheimer, L.~Pang, K.~Zhou, V.~Koch, J.~Randrup and H.~Stoecker, arXiv:1906.06562</p>

## Point Cloud network

<img class="plain" src="images/PCN_structure.png" alt="drawing" width="80%" align='center'/>

## Better than CNN and random guess

<img class="plain" src="images/sp_vs_maxwell_result.png" alt="drawing" width="70%" align='center'/>

## Nuclear Deformation
<img class="plain" src="images/deformed_nuclei_shapes.png" alt="drawing" width="50%" align='center'/>

## Nuclear deformation from heavy ion collisions
<img class="plain" src="images/deformation_network.png" alt="drawing" width="70%" align='center'/>

## Interpretation
<img class="plain" src="images/deformation_interpretation.png" alt="drawing" width="45%" align='center'/>

We developed the Attention Regression Mask algorithm for knowledge discovery.

## Nuclear Structure

- *Nuclear deformation*
- Neutron skin
- Charge distribution
- Pairing
- Alpha cluster


# Quantum Computing for nuclear physics

## Classical Computer: Bits and Gates

<img class="plain" src="images/classicial_bits.png" alt="drawing" width="70%" align='center'/>

## One Qubit
<img class="plain" src="images/one_qubit.png" alt="drawing" width="70%" align='center'/>

## Two Qubits
<img class="plain" src="images/two_qubits.png" alt="drawing" width="70%" align='center'/>

## Entanglement
<img class="plain" src="images/epr_state.png" alt="drawing" width="70%" align='center'/>

## Superposition for quantum Parallel
<img class="plain" src="images/big_hilbert_space.png" alt="drawing" width="70%" align='center'/>

## Quantum Gates: Not
<img class="plain" src="images/Quantum_Not_Gate.png" alt="drawing" width="70%" align='center'/>

## Pauli
<img class="plain" src="images/Quantum_YZ_Gate.png" alt="drawing" width="70%" align='center'/>

## Hadamard for superposition
<img class="plain" src="images/Quantum_Hadamard_Gate.png" alt="drawing" width="70%" align='center'/>

## CNOT for XOR
<img class="plain" src="images/Quantum_CNOT_Gate.png" alt="drawing" width="70%" align='center'/>

## H + CNOT for entanglement
<img class="plain" src="images/Quantum_Enganglement_Gate.png" alt="drawing" width="70%" align='center'/>

## Rotation 
<img class="plain" src="images/Quantum_Rotation_Gate.png" alt="drawing" width="70%" align='center'/>

## Nuclear shell model: curse of dimensionality

- E.g., 100 spin-orbital sites
- 25 neutron + 25 proton

$$ C_{100}^{25} \times C_{100}^{25} \approx 1.5\times 10^{215} $$

## Qubits for quantum simulations

$$ 800\ Qubits\ = 2^{800} \approx 6.7 \times 10^{240}\ states$$

## Variational Quantum Eigensolver (VQE)
<img class="plain" src="images/VQE_formula.png" alt="drawing" width="70%" align='center'/>

## Trial wave function
$$ \Psi(\theta) = \left[\mathrm{U_{entangle} U_{rotation}(\theta)} \right]^m |0\rangle $$

- $\mathrm{U_{entangle}}$ by CNOT gate
- $\mathrm{U_{rotation}} = \Pi_{i=1}^{n} Z(\theta_i) Y(\theta_{n+i}) Z(\theta_{2n+i})$ 
- n: the number of Qubits
- m: the depth of the quantum circuit


## VQE on IBM Quantum computer
<img class="plain" src="images/VQE_ibm_nature.png" alt="drawing" width="70%" align='center'/>

<p class='ref'> A. Kandala, A. Mezzacapo, K. Temme, M. Takita, M. Brink, J. M. Chow, and J. M. Gambetta, Hardware-efficient Variational Quantum Eigensolver for Small Molecules and Quantum Magnets, Nature 549, 242 (2017), and references therein. </p>

## VQE for Deuteron binding energy

<img class="plain" src="images/VQE_deuteron_ref.png" alt="drawing" width="70%" align='center'/>

## Fermionic Hamiltonian to Qubit Hamiltonian
<img class="plain" src="images/VQE_for_deuteron.png" alt="drawing" width="70%" align='center'/>

## Deuteron: 3 Qubits for 3 states
<img class="plain" src="images/VQE_Deuteron_H3.png" alt="drawing" width="70%" align='center'/>

## Problems of VQE
- Hamiltonian has too many decompositions
    - Brute force: $\sim 4^n$ terms, 
    - $n$ is the required number of qubits
    - Jordan-Wigner or Bravyi-Kitaev: $\sim M^4$ terms
    - M is the number of single particle states
- Too many measurements for each decomposition
- Representation power of the quantum circuit

# Deep Learning for VQMC

## Many electron Schrodinger equation

<img class="plain" src="images/DeepWF_2018_Ref.png" alt="drawing" width="50%" align='center'/>
<img class="plain" src="images/many_electron_harmiltonian.png" alt="drawing" width="33%" align='center'/>

- N electrons, M ions, Born-Oppenheimer approximation

## DeepWF

<img class="plain" src="images/DeepWF.png" alt="drawing" width="70%" align='center'/>

<img class="plain" src="images/variational_wf.png" alt="drawing" width="33%" align='center'/>
$$ \boldsymbol{a}_{0}^{\uparrow}\left(\boldsymbol{r}_{i}, \boldsymbol{r}_{j}\right)=\operatorname{Net}_{\mathrm{anti}}^{\uparrow}\left(\boldsymbol{r}_{i}, \boldsymbol{r}_{j}, r_{j i}\right)-\operatorname{Net}_{\mathrm{anti}}^{\uparrow}\left(\boldsymbol{r}_{j}, \boldsymbol{r}_{i}, r_{j i}\right)
$$

## DeepWF Results

<img class="plain" src="images/DeepWF_Res.png" alt="drawing" width="50%" align='center'/>

## More compact representation of the wavefunction

<p class="ref"> Ab-Initio Solution of the Many-Electron Schro ̈dinger Equation with Deep Neural Networks,
David Pfau, James S. Spencer, and Alexander G. de G. Matthews from DeepMind, arXiv:1909.02487v1 </p>

## Fermi Net

<img class="plain" src="images/FermiNet_DeepMind.png" alt="drawing" width="80%" align='center'/>

## Fermi Net for H10 chain

<img class="plain" src="images/H10_FermiNet.png" alt="drawing" width="70%" align='center'/>

## Summary

- Deep learning helps to decode the nuclear EoS and shape-deformation
- VQE: hybrid quantum-classical optimization for many-body-quantum system
- New possibility: deep learning for compact quantum wave function

# Backups

## How to train a neural network
- Initialize the parameters
- Choose an optimization algorithm
- Repeat the following steps:
    - Forward propagate an input
    - Compute the cost function
    - Compute the gradients of the cost with respect to parameters using back-propagation
    - Update each parameter using the gradients, according to the optimization algorithm

## Quantum De-Excitation
<img class="plain" src="images/quantum_deexcitation.png" alt="drawing" width="70%" align='center'/>

## Quantum De-Phasing
<img class="plain" src="images/quantum_dephasing.png" alt="drawing" width="70%" align='center'/>


## Jordan-Wigner transformation
<img class="plain" src="images/Jordan-Wigner-transformation.png" alt="drawing" width="70%" align='center'/>

## Measuring Pauli matrices
<img class="plain" src="images/Quantum_Measuring_Pauli.png" alt="drawing" width="70%" align='center'/>

## Measuring Pauli matrices
<img class="plain" src="images/Quantum_Measure_XY.png" alt="drawing" width="70%" align='center'/>

## Brute Force Hamiltonian decomposition
<img class="plain" src="images/VQE_Decompose.png" alt="drawing" width="70%" align='center'/>


