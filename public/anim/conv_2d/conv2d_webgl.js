let canvas;
let random_weights = [];

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

function setup() {
    createCanvas(windowWidth, windowHeight, WEBGL);
    ortho(-width, width, height, -height/2, 0, 1000);
    background(255);

    // grid shape
    shape = [5, 5];
    // text for each grid cell
    texts = [0, 1];
    // plot the grids
    rotateY(radians(0));

    grids(0, 0, -300, 400, 400, shape, texts, false);

    grids(0, 0, 300, 400, 400, [3, 3], texts, false);

    grids(0, 0, -200, 133, 133, [3, 3], texts, true);

}


function grids(posx, posy, posz, box_w, box_h, shape, texts, tozero) {
    push();

    if (tozero) {
        translate(posx, posy, posz);
    } else {
        translate(posx - box_w/2, posy-box_h/2, posz);
    }
    let rows = shape[0];
    let cols = shape[1];
    let r_gap = box_h / (rows + 2);
    let c_gap = box_w / (cols + 2);

    strokeWeight(5);
    stroke(color(20, 20, 20));

    for (let i=0; i<rows; i++) {
        for (let j=0; j<cols; j++) {
            let x = (j+1) * c_gap;
            let y = (i+1) * r_gap;
            rect(x, y, c_gap, r_gap);
            textSize(50);
            text("0", x+0.5*c_gap, y+0.5*r_gap, 0);
        }
    }



    pop();
}
