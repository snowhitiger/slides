let canvas;

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

function setup() {
    canvas = createCanvas(windowWidth, windowHeight);
    canvas.position(0, 0);
    canvas.style("z-index", "-1");
    strokeCap(ROUND);
    frameRate(1);
    shape = [6, 5];
}

/* draw 1D convolution neural network */
function draw() {
    background(255);
    stroke(color(20, 20, 20));
    textSize(30);

    dense_network(shape, 0, 0, windowWidth/3, 0.8*windowHeight);
    text("Densely connected", 0.05*width, height*7/8);

    locally_connected_network(shape, windowWidth/3, 0, windowWidth/3, 0.8*windowHeight);
    text("Locally connected", (0.05+1/3.0)*width, height*7/8);

    let idx = frameCount % 5;
    conv1d(shape, windowWidth*2/3, 0, windowWidth/3, 0.8*windowHeight, idx);
    text("Convolution network", (0.05+2/3.0)*width, height*7/8);
}

/* plot dense neural network, where "shape" is a list of 
 * number of neurons in each layer   */
function dense_network(shape, posx, posy, box_w, box_h) {
    push();
    translate(posx, posy);
    let neuron_size = box_h / 20;
    let num_layers = shape.length;
    let horizental_gap = box_w / (num_layers + 1);

    // plot links 
    strokeWeight(5);
    stroke(color(20, 20, 20));
    for (let l = 0; l < num_layers-1; l++) {
        let num_neurons_1 = shape[l];
        let num_neurons_2 = shape[l+1];

        let vertical_gap_1 = box_h / (num_neurons_1 + 1);
        let vertical_gap_2 = box_h / (num_neurons_2 + 1);

        let neuron_x_1 = horizental_gap * (l + 1);
        let neuron_x_2 = horizental_gap * (l + 2);

        for (let n1 = 0; n1 < num_neurons_1; n1++) {
            let neuron_y_1 = vertical_gap_1 * (n1 + 1);
            for (let n2 = 0; n2 < num_neurons_2; n2++) {
                let neuron_y_2 = vertical_gap_2 * (n2 + 1);
                line(neuron_x_1, neuron_y_1, neuron_x_2, neuron_y_2);
            }
        }
    }

    // plot neurons 
    strokeWeight(5);
    stroke(color(0, 0, 0));
    for (let l = 0; l < num_layers; l++) {
        let num_neurons = shape[l];
        let vertical_gap = box_h / (num_neurons + 1);
        let neuron_x = horizental_gap * (l + 1);
        for (let n = 0; n < num_neurons; n++) {
            let neuron_y = vertical_gap * (n + 1);
            ellipse(neuron_x, neuron_y, neuron_size);
        }
    }
    pop();
}


/* plot neural network with local connection, where "shape" is a list of 
 * number of neurons in each layer   
 * Notice: each layer must have 1 neuron fewer than its previous layer
 * */
function locally_connected_network(shape, posx, posy, box_w, box_h) {
    push();
    translate(posx, posy);
    let neuron_size = box_h / 20;
    let num_layers = shape.length;
    let horizental_gap = box_w / (num_layers + 1);

    // plot links 
    strokeWeight(5);
    stroke(color(20, 20, 20));
    for (let l = 0; l < num_layers-1; l++) {
        let num_neurons_1 = shape[l];
        let num_neurons_2 = shape[l+1];

        let vertical_gap_1 = box_h / (num_neurons_1 + 1);
        let vertical_gap_2 = box_h / (num_neurons_2 + 1);

        let neuron_x_1 = horizental_gap * (l + 1);
        let neuron_x_2 = horizental_gap * (l + 2);

        for (let n1 = 0; n1 < num_neurons_1-1; n1++) {
            // 2 neurons in the previous layer
            let neuron_y_11 = vertical_gap_1 * (n1 + 1);
            let neuron_y_12 = vertical_gap_1 * (n1 + 1 + 1);
            // is connected to 1 neuron in the next layer
            let neuron_y_2 = vertical_gap_2 * (n1 + 1);

            line(neuron_x_1, neuron_y_11, neuron_x_2, neuron_y_2);
            line(neuron_x_1, neuron_y_12, neuron_x_2, neuron_y_2);
        }
    }

    // plot neurons 
    strokeWeight(5);
    stroke(color(0, 0, 0));
    for (let l = 0; l < num_layers; l++) {
        let num_neurons = shape[l];
        let vertical_gap = box_h / (num_neurons + 1);
        let neuron_x = horizental_gap * (l + 1);
        for (let n = 0; n < num_neurons; n++) {
            let neuron_y = vertical_gap * (n + 1);
            ellipse(neuron_x, neuron_y, neuron_size);
        }
    }
    pop();
}


/* plot 1D convolution neural network with local connection, where "shape" is a list of 
 * number of neurons in each layer   
 * Notice: each layer must have 1 neuron fewer than its previous layer
 * */
function conv1d(shape, posx, posy, box_w, box_h, link_idx, kind) {
    push();
    translate(posx, posy);
    let neuron_size = box_h / 20;
    let num_layers = shape.length;
    let horizental_gap = box_w / (num_layers + 1);

    // plot links 
    strokeWeight(5);
    stroke(color(20, 20, 20));
    for (let l = 0; l < num_layers-1; l++) {
        let num_neurons_1 = shape[l];
        let num_neurons_2 = shape[l+1];

        let vertical_gap_1 = box_h / (num_neurons_1 + 1);
        let vertical_gap_2 = box_h / (num_neurons_2 + 1);

        let neuron_x_1 = horizental_gap * (l + 1);
        let neuron_x_2 = horizental_gap * (l + 2);

        let n1 = link_idx;

        // 2 neurons in the previous layer
        let neuron_y_11 = vertical_gap_1 * (n1 + 1);
        let neuron_y_12 = vertical_gap_1 * (n1 + 1 + 1);
        // is connected to 1 neuron in the next layer
        let neuron_y_2 = vertical_gap_2 * (n1 + 1);

        line(neuron_x_1, neuron_y_11, neuron_x_2, neuron_y_2);
        line(neuron_x_1, neuron_y_12, neuron_x_2, neuron_y_2);
    }

    // plot neurons 
    strokeWeight(5);
    stroke(color(0, 0, 0));
    for (let l = 0; l < num_layers; l++) {
        let num_neurons = shape[l];
        let vertical_gap = box_h / (num_neurons + 1);
        let neuron_x = horizental_gap * (l + 1);
        for (let n = 0; n < num_neurons; n++) {
            let neuron_y = vertical_gap * (n + 1);
            ellipse(neuron_x, neuron_y, neuron_size);
        }
    }
    pop();
}
