let canvas;
let h1;
let random_weights = [];
let random_weight;


function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

function setup() {
    canvas = createCanvas(windowWidth, windowHeight);
    canvas.position(0, 0);
    canvas.style("z-index", "-1");
    strokeCap(ROUND);
    frameRate(1);

    random_weights[0] = -0.15;
    random_weights[1] =  0.45;
    random_weights[2] =  0.05;
    random_weights[3] = -0.9;
    random_weights[4] =  0.65;
}

function draw() {
    background(255);

    textSize(50);
    text("One artificial neuron", 0.4*width, 0.1*height);
    
    let output_neuron_value = 0.0;

    // plot input neurons 
    let neuron_size = height / 20;
    let output_y = 3/7 * height;
    for (let i=0; i<5; i++) {
        let y = height * (i+1) / 7.0;
        let x = width / 4;
        let weight_color = color(0, 0, 0);
        random_weight = random_weights[i];
        if (random_weight > 0) {
            weight_color = color(200, 0, 0);
        } else {
            weight_color = color(0, 0, 0);
        }
        stroke(weight_color);

        line_width = abs(random_weight*12)+1;

        // plot links
        strokeWeight(line_width);
        line(x, y, width/2, output_y);

        stroke(color(0, 0, 0));
        // plot text for xi
        input_value = random(0, 1);
        strokeWeight(1);
        textSize(30);
        let str = "X" + i.toString() + "=" + input_value.toFixed(3).toString();
        text(str,  x - 200, y);

        push();
        translate(0.375*width, 0.50*(y+output_y)-10);
        rotate(PI/6.0 - i * PI / 12.0);
        let wi = "W" + i.toString();
        
        textSize(30);
        if (random_weight > 0) {
            wi = wi + " >0";
        } else {
            wi = wi + " <0";
        }

        text(wi, 0, 0);
        pop();

        // plot neurons
        strokeWeight(5);
        input_neuron_size = neuron_size * (input_value+0.1);
        ellipse(x, y, input_neuron_size);

        output_neuron_value += input_value * random_weight;

    }

    strokeWeight(1);
    stroke(color(0, 0, 0));
    xw = width*0.5;
    yw = height/7;

    // plot bias to output link
    strokeWeight(5);
    stroke(color(0, 0, 0));
    line(0.5*width, 1.5*output_y, 0.5*width, output_y);

    // plot bias neuron
    strokeWeight(5);
    stroke(color(0, 0, 0));
    ellipse(width/2, 1.5*output_y, neuron_size);

    strokeWeight(1);
    text("bias", 0.5*width-10, 1.7*output_y);
    text("b", 0.5*width-5, 1.52*output_y);


    // plot z to h activation link
    stroke(color(0, 0, 0));
    strokeWeight(5);
    line(0.5*width, output_y, 0.6*width, output_y);

    // plot output neuron
    strokeWeight(5);
    stroke(color(0, 0, 0));
    ellipse(width/2, output_y, neuron_size);

    if (output_neuron_value > 0) {
        push();
        noStroke();
        fill(color(255, 0, 0));
        ellipse(width/2, output_y, neuron_size);
        pop();
    }

    // write output neuron value
    stroke(color(0, 0, 0));
    strokeWeight(1);
    text("Z = "+output_neuron_value.toFixed(3).toString(), 0.48*width, output_y-50);
    //text("h= "+abs(output_neuron_value).toFixed(3).toString(), 0.48*width, output_y+50);

    // plot z to h activation link
    stroke(color(0, 0, 0));
    strokeWeight(5);
    
    let box_w = 0.2*width;
    let box_h = box_w;
    let box_wstart = 0.6 * width;
    let box_hstart = output_y - 0.5 * box_h;
    rect(box_wstart, box_hstart, box_w, box_w);

    // plot activation function
    stroke(color(0, 0, 0));
    strokeWeight(0.2);
    push();
    translate(box_wstart, box_hstart);
    line(0, 0.5*box_w, box_w, 0.5*box_w);
    line(0.5*box_w, 0, 0.5*box_w, box_w);
    //Draw ReLU
    strokeWeight(5);
    line(0.1*box_w, 0.5*box_w, 0.5*box_w, 0.5*box_w);
    line(0.5*box_w, 0.5*box_w, 0.9*box_w, 0.1*box_w);

    strokeWeight(1);
    text("Z", 0.8*box_w, 0.5*box_h);
    text("h", 0.45*box_w, 0.2*box_h);
    text("ReLU Activation function", 0.1*box_w, 1.2*box_h);

    if (output_neuron_value > 0) {
        stroke(color(255, 0, 0));
        line(0.5*box_w, 0.5*box_w, 0.9*box_w, 0.1*box_w);

        strokeWeight(1);
        text("Activated!", 0.3*box_w, 0.8*box_h);

    }
    // text output 
    stroke(color(0, 0, 0));
    strokeWeight(5);
    line(box_w, 0.5*box_h, 1.4*box_w, 0.5*box_h);
    strokeWeight(1);
    text("output = h", 1.45*box_w, 0.5*box_h);

    stroke(color(0, 0, 0));
    strokeWeight(1);
    text("h = ReLU(Z) = ReLU(X W + b)", 0, -0.1*box_h);
    pop();


}
