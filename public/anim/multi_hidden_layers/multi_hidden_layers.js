let canvas;
let h1;
let random_weights = [];
let random_weight;

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}

function setup() {
    canvas = createCanvas(windowWidth, windowHeight);
    canvas.position(0, 0);
    canvas.style("z-index", "-1");
    strokeCap(ROUND);
    frameRate(1);
    background(255);

    shape = [3, 6, 4, 6, 5];
    dense_network(shape, 0, 0, windowWidth, windowHeight);
    stroke(color(20, 20, 20));
    textSize(50);
    text("Multiple hidden layers", width*2.3/6, height/10);
}

/* plot dense neural network, where "shape" is a list of 
 * number of neurons in each layer   */
function dense_network(shape, posx, posy, box_w, box_h) {
    push();
    translate(posx, posy);
    let neuron_size = box_h / 20;
    let num_layers = shape.length;
    let horizental_gap = box_w / (num_layers + 1);

    // plot links 
    strokeWeight(5);
    stroke(color(20, 20, 20));
    for (let l = 0; l < num_layers-1; l++) {
        let num_neurons_1 = shape[l];
        let num_neurons_2 = shape[l+1];

        let vertical_gap_1 = box_h / (num_neurons_1 + 1);
        let vertical_gap_2 = box_h / (num_neurons_2 + 1);

        let neuron_x_1 = horizental_gap * (l + 1);
        let neuron_x_2 = horizental_gap * (l + 2);

        for (let n1 = 0; n1 < num_neurons_1; n1++) {
            let neuron_y_1 = vertical_gap_1 * (n1 + 1);
            for (let n2 = 0; n2 < num_neurons_2; n2++) {
                let neuron_y_2 = vertical_gap_2 * (n2 + 1);
                line(neuron_x_1, neuron_y_1, neuron_x_2, neuron_y_2);
            }
        }
    }

    // plot neurons 
    strokeWeight(5);
    stroke(color(0, 0, 0));
    for (let l = 0; l < num_layers; l++) {
        let num_neurons = shape[l];
        let vertical_gap = box_h / (num_neurons + 1);
        let neuron_x = horizental_gap * (l + 1);
        for (let n = 0; n < num_neurons; n++) {
            let neuron_y = vertical_gap * (n + 1);
            ellipse(neuron_x, neuron_y, neuron_size);
        }
    }
    pop();
}
